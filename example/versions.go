//+build swscale,avfilter,avdevice goav_all

package main

import (
	"log"

	"gitlab.com/T4cC0re/goav/avcodec"
	"gitlab.com/T4cC0re/goav/avdevice"
	"gitlab.com/T4cC0re/goav/avfilter"
	"gitlab.com/T4cC0re/goav/avformat"
	"gitlab.com/T4cC0re/goav/avutil"
	"gitlab.com/T4cC0re/goav/swresample"
	"gitlab.com/T4cC0re/goav/swscale"
)

func main() {

	// Register all formats and codecs
	avformat.AvRegisterAll()
	avcodec.AvcodecRegisterAll()

	log.Printf("AvFilter Version:\t%v", avfilter.AvfilterVersion())
	log.Printf("AvDevice Version:\t%v", avdevice.AvdeviceVersion())
	log.Printf("SWScale Version:\t%v", swscale.SwscaleVersion())
	log.Printf("AvUtil Version:\t%v", avutil.AvutilVersion())
	log.Printf("AvCodec Version:\t%v", avcodec.AvcodecVersion())
	log.Printf("Resample Version:\t%v", swresample.SwresampleLicense())

}
