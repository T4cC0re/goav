module gitlab.com/T4cC0re/goav

go 1.14

require (
	github.com/gosuri/uilive v0.0.0-20170323041506-ac356e6e42cd // indirect
	github.com/gosuri/uiprogress v0.0.0-20170224063937-d0567a9d84a1 // indirect
)
